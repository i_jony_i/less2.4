#!/bin/bash
job_id=$(curl --globoff --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/25703988/jobs" | jq '[ .[] | select( .stage=="build") | .id ] | .[0]')
echo $job_id
curl --location --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/25703988/jobs/${job_id}/artifacts/log" -o log

